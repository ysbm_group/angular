/**
 * ProductListCtrl - controller
 */

function ProductListCtrl(config, toastr, $timeout, $scope, $uibModal, $http, $log) {
    $scope.itemNumber = [];
    var toPost = [];

    $scope.sortType = 'title.raw';
    $scope.sortReverse = 'asc';

    $scope.viewTheProduct = function (data) {
        $uibModal.open({
            animation:   true,
            templateUrl: 'views/product/previewModal.html',
            controller:  'PreviewWindowCtrl',
            size:        'lg',
            resolve:     {
                doc: function () {
                    return data;
                }
            }
        });
    };
    $scope.range = {from: 0, to: 10};
    $scope.processItems = function (doc, action) {
        for (var i = 0; i < doc.length; i++) {
            if ($scope.itemNumber[i]) {
                toPost.push(doc[i]._id);
            }
        }
        $http.post(config.apiUrl + "/product/" + action, toPost).then(
            function success(response) {
                console.log(response);
                $scope.itemNumber = [];
                toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});

                setTimeout(function () {
                    $scope.indexVM.refresh(false);
                }, 5000);
            },
            function error(response) {
                console.log("Data NOT sent", JSON.stringify(response));
                Bugsnag.notify("API communication error", response.message, response);
                toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
            }
        );
    };
    $scope.processItem = function (doc, action, item) {
        toPost.push(doc[item]._id);
        $http.post(config.apiUrl + "/product/" + action, toPost).then(
            function success(response) {
                console.log(response);
                $scope.itemNumber = [];
                toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});

                setTimeout(function () {
                    $scope.indexVM.refresh(false);
                }, 5000);
            },
            function error(response) {
                console.log("Data NOT sent", JSON.stringify(response));
                Bugsnag.notify("API communication error", response.message, response);
                toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
            }
        );
    };
    $scope.processItemPick = function (value, length) {
        for (var i = 0; i < length; i++) {
            $scope.itemNumber[i] = value;
        }
    };

    $scope.mass_edit_value = 0;
    $scope.mass_edit_type = 'stock';
    $scope.massEditTemplate = 'views/product/massEditTemplate.html';
    $scope.popoverIsOpen = false;

    $scope.popoverAction = function (action) {
        $scope.popoverIsOpen = action;
    };

    $scope.massEdit = function (doc, type, value) {
        var postData = {};
        var items = [];
        for (var i = 0; i < doc.length; i++) {
            if ($scope.itemNumber[i]) {
                items.push(doc[i]._id);
            }
        }

        postData.products = items;

        if (type == "stock") {
            postData.newStockAmount = value;
            $http.post(config.apiUrl + "/product/mass/stock", postData).then(
                function success(response) {
                    console.log(response);
                    $scope.itemNumber = [];
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});

                },
                function error(response) {
                    console.log("Data NOT sent", JSON.stringify(response));
                    Bugsnag.notify("API communication error", JSON.stringify(response));
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                }
            );
        }
        else if (type == "shipping_fee") {
            postData.newShippingFee = value;
            $http.post(config.apiUrl + "/product/mass/shippingfee", postData).then(
                function success(response) {
                    console.log(response);
                    $scope.itemNumber = [];
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});

                },
                function error(response) {
                    console.log("Data NOT sent", JSON.stringify(response));
                    Bugsnag.notify("API communication error", JSON.stringify(response));
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                }
            );
        }

    };

    $scope.stockChangeModal = function (productId) {
        var modalInstance = $uibModal.open({
            animation:   true,
            templateUrl: 'views/product/editStockModal.html',
            controller:  'productChangeModalCtrl',
            size:        'lg',
            resolve:     {
                item: function () {
                    return productId;
                }
            }
        });

        modalInstance.result.then(function (product) {
            setTimeout(function () {
                $scope.indexVM.refresh(false);
            }, 5000);
        });
    };
}
