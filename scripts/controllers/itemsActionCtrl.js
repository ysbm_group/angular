function itemsActionCtrl(config, $http, toastr, $scope, $uibModalInstance, action, items, checklist) {

    $scope.view = action;
    $scope.loading = false; // stop loading
    $scope.data = items;
    $scope.date = items.added;
    $scope.products = items.orderedProducts;
    console.log($scope.products);

    if (items.cancelReason === 'OUT_OF_STOCK' || items.cancelReason === 'INVALID_ADDRESS') {
        $scope.canceller = 'merchant';
    }
    else {
        $scope.canceller = 'user';
    }

    if (action == 'Refund') {
        $scope.invoiceNumber = items.subOrderId.substr(0, 8);
        // for (var c = 0; c < 6; c++){
        //     var invoiceNumber = getRandomInt(0, 10);
        //     $scope.invoiceNumber = $scope.invoiceNumber + invoiceNumber;
        // }

        $scope.products = items.cancelledProducts;
        $http.get(config.apiUrl + '/admin/shop/pl-testmerchant', {cache: true}).then(function success(response) {
            $scope.merchantInfo = response.data;
            console.log(response.data);
        });
    }

    $scope.toShow = checklist;
    if ($scope.toShow == 'checkedAll') {
        $scope.toShow = [];
        for (var j = 0; j < $scope.products.length; j++) {
            $scope.toShow.push(true);
        }
    }

    $scope.close = function (response) {
        if (response) {
            $uibModalInstance.close(response.data);
        }
        else {
            $uibModalInstance.close();
        }
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $http.get(config.apiUrl + "/data/couriers", {cache: true}).then(function success(response) {
        $scope.shipmentProviders = response.data;
        console.log(response.data);
    });

    $scope.getShipmentProvider = function () {
        // $http.get(config.apiUrl + "/data/couriers/" + $scope.shipmentId, {cache: true}).then(function success(response) {
        //     $scope.shipmentProvider = response.data;
        //     console.log(response.data);
        // });
    };

    var toPost = [];
    $scope.sendTheData = function (action) {
        $scope.loading = true; // start loading
        if (action == 'Ship items') {
            toPost = {};
            toPost.items = [];
            toPost.shipmentId = $scope.shipmentId;
            toPost.shipmentProvider = $scope.shipmentProvider;
        }

        for (var i = 0; i < $scope.products.length; i++) {
            if ($scope.toShow[i] && (action === 'Cancel items')) {
                toPost.push({
                    "productId": $scope.products[i].productId,
                    "sku":       $scope.products[i].sku,
                    "amount":    $scope.products[i].amount,
                    "reason":    $scope.reason
                });
            } else if ($scope.toShow[i] && (action === 'Ship items')) {
                console.log(toPost);
                toPost.items.push({
                    "productId": $scope.products[i].productId,
                    "sku":       $scope.products[i].sku,
                    "amount":    $scope.products[i].amount
                });
            }
        }
        if (action === 'Refund') {
            console.log(toPost);
            $http.get(config.apiUrl + "/admin/refund/" + items.refundId + "/close").then(
                function success(response) {
                    console.log("Data sent");
                    toPost = [];
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});
                    $scope.close(response);
                },
                function error(response) {
                    $scope.loading = false; // stop loading
                    Bugsnag.notify("API communication error", response.message, response);
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                });
        } else if (action === 'Cancel items') {
            console.log(toPost);
            $http.post(config.apiUrl + "/admin/order/" + items.subOrderId + "/cancel", toPost).then(
                function success(response) {
                    console.log("Data sent");
                    toPost = [];
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});
                    $scope.close(response);
                },
                function error(response) {
                    $scope.loading = false; // stop loading
                    Bugsnag.notify("API communication error", response.message, response);
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                });
        } else {
            console.log(toPost);
            $http.post(config.apiUrl + "/admin/order/" + items.subOrderId + "/ship", toPost).then(
                function success(response) {
                    console.log("Data sent", response);
                    toPost = [];
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});
                    $scope.close(response);
                },
                function error(response) {
                    $scope.loading = false; // stop loading
                    Bugsnag.notify("API communication error", response.message, response);
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                });
        }
    };
}   
