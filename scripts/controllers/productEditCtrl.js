/**
 * ProductEditCtrl - controller
 */
function ProductEditCtrl(config, $scope, $http, $stateParams, Upload, $timeout, $state, toastr, auth) {
    window.sco = $scope;

    $scope.edit = $state.$current.url.prefix.indexOf('edit');
    $scope.edition = false;
    $scope.canSave = false;

    $scope.Product = {};

    $scope.pageTitle = "Add new product";
    $scope.Product.productVariationsUntouchebles = [];
    $scope.Product.productColorsUntouchebles = 0;
    $scope.Product.productMaterialsUntouchebles = 0;

    $scope.Product.colorsSelected = [
        {
            color:     '',
            touchable: true
        }
    ];
    $scope.Product.materialsSelected = [
        {
            material:   '',
            percentage: null,
            touchable:  true
        }
    ];
    $scope.Product.colorsAndImages = {
        ready:  false,
        colors: [],
        images: []
    };
    $scope.Product.productVariations = [];
    $scope.Product.productImages = [];
    $scope.Product.mainCategoriesFinal = [];

    $scope.Product.sizesSelected = [];

    $scope.categoriesList = [];
    $scope.mainCategories = [];

    if ($stateParams.productID) {
        $scope.pageTitle = "Edit product: " + $stateParams.productID;
        $http.get(config.apiUrl + "/product/" + $stateParams.productID + "/details", {cache: false}).then(function success(response) {
                
                var product = response.data;
                console.log(product);
                if (product) {
                    $scope.Product = product;
                    $scope.edition = true;

                    if (product.categories) {
                        loadCategories();
                        $scope.Product.gender = product.categories[0].substr(0, product.categories[0].indexOf('|'));
                        var categories = product.categories[0].substr(product.categories[0].indexOf('|') + 1, product.categories[0].length + 1);
                        $scope.Product.firstCategory = categories.substr(0, categories.indexOf('|'));
                        $scope.firstCategories($scope.Product.gender);
                        categories = categories.substr(categories.indexOf('|') + 1);
                        $scope.Product.secondCategory = (categories.indexOf('|') == -1) ? categories : categories.substr(0, categories.indexOf('|'));

                        $scope.secondCategories($scope.Product.gender, $scope.Product.firstCategory);
                        $scope.finalCategories($scope.Product.gender, $scope.Product.firstCategory, $scope.Product.secondCategory);

                        for (var i = 0; i < product.categories.length; i++) {
                            for (var j = 0; j < $scope.Product.mainCategoriesFinal.length; j++) {
                                if (product.categories[i].indexOf($scope.Product.mainCategoriesFinal[j].text) > -1) {
                                    $scope.Product.mainCategoriesFinal[j].ticked = true;
                                }
                            }
                        }
                    }

                    if (product.material) {
                        $scope.Product.materialsSelected = [];
                        angular.forEach(product.material, function (material, key) {
                            $scope.Product.materialsSelected.push({
                                material:   material.name,
                                percentage: material.percentage,
                                touchable:  false
                            });
                        });
                    }

                    if (product.images) {
                        $scope.Product.colorsAndImages = {
                            ready:  false,
                            colors: [],
                            images: []
                        };
                        $scope.Product.productImages = [];
                        angular.forEach(product.images, function (image, key) {
                            $scope.Product.productImages.push({
                                name:     image,
                                location: "//some_url/"
                            });
                        });
                        $scope.Product.colorsAndImages.images.push({
                            color:  'Uploaded images',
                            images: $scope.Product.productImages
                        });
                    }

                    if (product.variations) {

                        $scope.Product.colorsSelected = [];

                        //if ($scope.mainCategories[i].value === product.categories[0]) {
                        //    $scope.loadSizes($scope.mainCategories[i].sizeGuide);
                        //}
                        var sizeTypeHelper = product.variations[0].sizeSelect.substr(0, product.variations[0].sizeSelect.indexOf(' '));
                        switch (sizeTypeHelper) {
                            case "UK":
                            case "EU":
                            case "US":
                                $scope.Product.sizesType = sizeTypeHelper;
                                break;
                            default:
                                $scope.Product.sizesType = "International";
                        }

                        $scope.getSizesForType($scope.Product.sizesType);
                        var sizesSelected = [];

                        angular.forEach(product.variations, function (variation, key) {
                            var check_color = true;
                            var check_size = true;
                            var check_color_image = true;
                            var split_size = variation.sizeSelect;
                            if ($scope.Product.sizesType !== "International") {
                                split_size = variation.sizeSelect.split(" ")[1];
                            }

                            variation.size = split_size;

                            angular.forEach($scope.Product.colorsSelected, function (selectedColor, k) {
                                if (variation.color == selectedColor.color) {
                                    check_color = false;
                                }
                            });
                            if (check_color) {
                                $scope.Product.colorsSelected.push({
                                    color:     variation.color,
                                    touchable: false
                                });
                            }

                            angular.forEach(sizesSelected, function (selectedSize, key) {
                                if (split_size === selectedSize) {
                                    check_size = false;
                                }
                            });
                            if (check_size) {
                                sizesSelected.push(split_size);
                            }

                            angular.forEach($scope.Product.colorsAndImages.colors, function (color, key) {
                                if (variation.color == color.color) {
                                    check_color_image = false;
                                }
                            });
                            if (check_color_image) {
                                var images = [];
                                angular.forEach(variation.images, function (image, key) {
                                    images.push({
                                        name:     image,
                                        location: "//some_url/"
                                    });
                                });
                                $scope.Product.colorsAndImages.colors.push({
                                    color:  variation.color,
                                    images: images
                                });
                            }
                        });

                        angular.forEach($scope.Product.sizesList, function (size, key) {
                            angular.forEach(sizesSelected, function (selected, k) {
                                if (size.text == selected) {
                                    size.disabled = true;
                                    size.ticked = true;
                                }
                            });
                        });

                        $scope.Product.productVariationsUntouchebles = product.variations;
                        $scope.Product.productColorsUntouchebles = $scope.Product.colorsSelected.length;
                        $scope.Product.productMaterialsUntouchebles = product.material.length;

                        $scope.colorDisable(true);
                        $scope.materialDisable();
                    }
                } else {
                    $scope.noData = true;
                }
            }, function error(response) {
                Bugsnag.notify("DataNotLoaded", "Data for product " + $stateParams.productID + " not loaded correctly.", response);
                // FIXME Without the data makes no sense to view and edit the product!
            }
        );
    }

    loadCategories();
    function loadCategories() {
        $http.get('categories.json', {cache: true}).then(function (response) {
            $scope.mainCategories = response.data;
        });
    }

    var merchant = auth.profile.merchantId; // TODO Change that do dynamic value from Auth0
    $http.get(config.apiUrl + "/admin/shop/" + merchant, {cache: true}).then(function (response) {
        $scope.Merchant = response.data;
    }, function error(response) {
        Bugsnag.notify("DataNotLoaded", "Data for merchant '" + merchant + "' not loaded correctly.", response);
        // FIXME Without the merchant data makes no sense to view and edit the product!
    });

    $scope.simplePattern = /[0-9a-zA-Z\.\,\`()\:-\s]{12,75}/;
    $scope.extendedPattern = /[0-9a-zA-Z\s\.\,\`()\:-;"]{12,8000}/;
    $scope.pricePattern = /[0-9\.]/;

    $scope.firstCategories = function (gender) {
        $scope.Product.gender = gender;
        $scope.mainCategoriesFiltered = [];
        $scope.mainCategoriesFilteredAgain = [];
        $scope.Product.mainCategoriesFinal = [];
        $scope.categoriesList = [];
        var pushing = true;
        for (var i = 0; i < $scope.mainCategories.length; i++) {
            pushing = true;
            if ($scope.mainCategories[i].text.indexOf(gender) > -1) {
                var pushed = $scope.mainCategories[i].text.substr(gender.length + 1);
                pushed = pushed.substr(0, pushed.indexOf('|'));
                for (var j = 0; j < $scope.mainCategoriesFiltered.length; j++) {
                    if ($scope.mainCategoriesFiltered[j] == pushed) {
                        pushing = false;
                    }
                }
                if (pushing && pushed.length > 1) {
                    $scope.mainCategoriesFiltered.push(pushed);
                }
            }
        }
    };

    $scope.secondCategories = function secondCategories(gender, firstCategory) {
        $scope.mainCategoriesFilteredAgain = [];
        $scope.Product.mainCategoriesFinal = [];
        $scope.categoriesList = [];
        var pushing = true;

        for (var i = 0; i < $scope.mainCategories.length; i++) {
            pushing = true;
            var incomeString = gender + '|' + firstCategory;
            if ($scope.mainCategories[i].text.indexOf(incomeString) > -1) {
                var pushed = $scope.mainCategories[i].text.substr(incomeString.length + 1);
                pushed = (pushed.indexOf('|') > -1) ? pushed.substr(0, pushed.indexOf('|')) : pushed;
                for (var j = 0; j < $scope.mainCategoriesFilteredAgain.length; j++) {
                    if ($scope.mainCategoriesFilteredAgain[j] == pushed) {
                        pushing = false;
                    }
                }
                if (pushing && pushed.length > 1) {
                    $scope.mainCategoriesFilteredAgain.push(pushed);
                }
            }
        }
    };

    $scope.finalCategories = function finalCategories(gender, firstCategory, secondaryCategory) {
        $scope.Product.mainCategoriesFinal = [];
        $scope.categoriesList = [];
        var pushing = true;
        var incomeString = gender + '|' + firstCategory + '|' + secondaryCategory + '|';
        for (var i = 0; i < $scope.mainCategories.length; i++) {
            pushing = true;

            if ($scope.mainCategories[i].text.indexOf(incomeString) > -1) {
                var pushed = {
                    text:   $scope.mainCategories[i].text.substr(incomeString.length),
                    ticked: false
                };

                for (var j = 0; j < $scope.Product.mainCategoriesFinal.length; j++) {
                    if ($scope.Product.mainCategoriesFinal[j].text == pushed.text) {
                        pushing = false;
                    }
                }
                if (pushing && pushed.text.length > 1) {
                    $scope.Product.mainCategoriesFinal.push(pushed);
                }
            }
            if ($scope.mainCategories[i].value == gender + '|' + firstCategory + '|' + secondaryCategory) {
                $scope.loadColors();
                $scope.loadSizes($scope.mainCategories[i].sizeGuide);
                $scope.loadMaterials();
            }
        }
        console.log($scope.Product.mainCategoriesFinal);
        if ($scope.Product.mainCategoriesFinal.length === 0) {
            $scope.categoriesList.push({
                "text":  gender + '|' + firstCategory + '|' + secondaryCategory,
                "value": gender + '|' + firstCategory + '|' + secondaryCategory
            });
        }
    };

    $scope.loadSizes = function (size_guide) {
        $scope.sizesGuide = {};
        $scope.Product.sizesTypes = [];
        var all_sizes = {
            'women-tops':       {
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'XL', 'L/XL', 'One size'],
                'UK':            [4, 6, 8, 10, 12, 14, 16, 18, 20, 'One size'],
                'US':            [0, 2, 4, 6, 8, 10, 12, 14, 16, 'One size'],
                'EU':            [32, 34, 36, 38, 40, 42, 44, 46, 48, 'One size']
            },
            'women-bottoms':    {
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'XL', 'L/XL', 'One size'],
                'UK':            [4, 6, 8, 10, 12, 14, 16, 18, 20, 'One size'],
                'US':            [0, 2, 4, 6, 8, 10, 12, 14, 16, 'One size'],
                'EU':            [32, 34, 36, 38, 40, 42, 44, 46, 48, 'One size']
            },
            'women-trousers':   {
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'XL', 'L/XL', 'One size'],
                'UK':            [4, 6, 8, 10, 12, 14, 16, 18, 20, 'One size'],
                'US':            [0, 2, 4, 6, 8, 10, 12, 14, 16, 'One size'],
                'EU':            [32, 34, 36, 38, 40, 42, 44, 46, 48, 'One size']
            },
            'bra-sizes':        {
                'UK': ['30A', '30B', '30C', '30D', '30DD', '32A', '32B', '32C', '32D', '32DD', '32E', '34A',
                    '34B', '34C', '34D', '34DD', '34E', '34F', '36A', '36B', '36C', '36D', '36DD', '36E', '38B', 'One size'],
                'US': ['65A', '65B', '65C', '65D', '65E', '70A', '70B', '70C', '70D', '70E', '70F', '75A', '75B',
                    '75C', '75D', '75E', '75F', '75G', '80A', '80B', '80C', '80D', '80E', '80F', '85B', 'One size'],
                'EU': ['30A', '30B', '30C', '30D', '30DD', '32A', '32B', '32C', '32D', '32DD', '32DDD', '34A',
                    '34B', '34C', '34D', '34DD', '34DDD', '34F', '36A', '36B', '36C', '36D', '36DD', '36DDD', '38B', 'One size']
            },
            'women-shoes':      {
                'UK': [2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10],
                'US': [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12],
                'EU': [35, 35, 35.5, 36, 36.5, 37, 37.5, 38, 38.5, 39, 39.5, 40, 40.5, 41, 41.5, 42, 42.5]
            },
            'men-tops':         {
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'XL/XXL', 'XXL', 'One size'],
                'UK':            [32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 'One size'],
                'US':            [32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 'One size'],
                'EU':            [42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 'One size']
            },
            'men-bottoms':      {
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'One size'],
                'UK':            [26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 'One size'],
                'US':            [26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 'One size'],
                'EU':            [42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 'One size']
            },
            'men-trousers':     { // !!! NEW !!!
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'One size'],
                'UK':            [26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 'One size'],
                'US':            [26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 'One size'],
                'EU':            [42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 'One size']
            },
            'men-shoes':        {
                'UK': [5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5],
                'US': [6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14],
                'EU': [39, 39, 40, 40.5, 41, 41.5, 42, 42.5, 43, 43.5, 44, 44.5, 45, 45.5, 46, 46.5, 47]
            },
            'scarves':          { // !!! NEW !!!
                'International': ['XS', 'X/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'One size']
            },
            'belts':            { // !!! NEW !!!
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'XL/XXL', 'XXL', 'One size'],
                'UK':            [28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 'One size'],
                'US':            [28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 'One size'],
                'EU':            [70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 'One size']
            },
            'bags':             { // !!! NEW !!!
                'International': ['XS', 'X/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'One size']
            },
            'women-hats':       { // !!! NEW !!!
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'One size']
            },
            'men-hats':         { // !!! NEW !!!
                'International': ['XS', 'XS/S', 'S', 'S/M', 'M', 'M/L', 'L', 'L/XL', 'XL', 'One size']
            },
            'women-rings':      { // !!! NEW !!!
                'International': ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'One size'],
                'UK':            ['H 1/2', 'K 1/2', 'M 1/2', 'P 1/2', 'R 3/4', 'U', 'One size'],
                'US':            ['4', '5 3/8', '6 1/2', '8', '9', '10 1/4', 'One size'],
                'EU':            [47, 50, 53, 57, 60, 63, 'One size']
            },
            'men-rings':        { // !!! NEW !!!
                'International': ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'One size'],
                'UK':            ['P 1/2', 'R 3/4', 'U', 'W 3/4', 'Z', 'One size'],
                'US':            ['8', '9', '10 1/4', '11 3/8', '12 1/2', 'One size'],
                'EU':            [57, 60, 63, 66, 69, 'One size']
            },
            'bracelets':        { // !!! NEW !!!
                'International': ['XS', 'S', 'M', 'L', 'XL', 'One size']
            },
            'women-necklaces':  { // !!! NEW !!!
                'International': ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL', '3XL', 'One size']
            },
            'men-necklaces':    { // !!! NEW !!!
                'International': ['S', 'M', 'L', 'XL', 'One size']
            },
            'hair-accessories': { // !!! NEW !!!
                'International': ['One size']
            }
        };

        var json_sizes = JSON.stringify(all_sizes);
        var sizesGuides = JSON.parse(json_sizes);
        console.log(json_sizes);

        angular.forEach(sizesGuides, function (sizesGuide, key) {
            if (key === size_guide) {
                $scope.sizesGuide = sizesGuide;
                angular.forEach(sizesGuide, function (sizes, sizesType) {
                    $scope.Product.sizesTypes.push(sizesType);
                });
            }
        });
        // return $http.get(config.apiUrl + '/data/dictionary/size-' + $scope.sizesKey, {cache: true}).then(function (response) {
        //     console.log(response.data);
        //     var sizesList = response.data;
        //     $scope.sizesList = [];
        //     for (var key in sizesList) {
        //         var value = sizesList[key];
        //         $scope.sizesList.push({
        //             text:     value,
        //             ticked:   false,
        //             disabled: false
        //         });
        //     }
        // });
    };
    $scope.getSizesForType = function (sizes_type) {
        $scope.Product.sizesType = sizes_type;
        $scope.Product.sizesList = [];
        angular.forEach($scope.sizesGuide, function (sizes, sizesType) {
            if (sizesType === sizes_type) {
                angular.forEach(sizes, function (size, key) {
                    $scope.Product.sizesList.push({
                        text:     size,
                        ticked:   false,
                        disabled: false
                    });
                });
            }
        });
    };

    $scope.loadColors = function () {
        $http.get(config.apiUrl + '/data/dictionary/colors', {cache: true}).then(function (response) {
            var colorsList = response.data;
            $scope.colorsList = [];

            angular.forEach(colorsList, function (value, key) {
                $scope.colorsList.push({color: value, picked: false});
            });
            $scope.colorDisable(false);
        });
    };
    $scope.colorDisable = function (generate) {
        if ($scope.colorsList && $scope.Product.colorsSelected) {
            for (var z = 0; z < $scope.colorsList.length; z++) {
                $scope.colorsList[z].picked = false;
            }
            for (var j = 0; j < $scope.Product.colorsSelected.length; j++) {
                for (var i = 0; i < $scope.colorsList.length; i++) {
                    if ($scope.colorsList[i].color == $scope.Product.colorsSelected[j].color) {
                        $scope.colorsList[i].picked = true;
                    }
                }
            }
        }
        if (generate) {
            $scope.generateVariations();
            $scope.generateColorsImages();
        }
    };

    $scope.addMoreColors = function () {
        $scope.Product.colorsSelected.push({
            color:     '',
            touchable: true
        });
    };
    $scope.loadMaterials = function () {
        return $http.get(config.apiUrl + '/data/dictionary/materials', {cache: true}).then(function (response) {
            var materialsList = response.data;
            $scope.materialsList = [];
            for (var key in materialsList) {
                var value = materialsList[key];
                $scope.materialsList.push({text: value, picked: false});
            }
        });
    };
    $scope.materialDisable = function () {
        if ($scope.materialsList && $scope.Product.materialsSelected) {
            angular.forEach($scope.materialsList, function (material, key) {
                material.picked = false;
                angular.forEach($scope.Product.materialsSelected, function (m, k) {
                    if (material.text == m.material) {
                        material.picked = true;
                    }
                });
            });
        }
    };
    $scope.percentageCheck = function () {
        $scope.percentageChecker = 0;
        for (var i = 0; i < $scope.Product.materialsSelected.length; i++) {
            $scope.percentageChecker = $scope.Product.materialsSelected[i].percentage + $scope.percentageChecker;
        }
    };
    $scope.addMoreMaterials = function () {
        $scope.Product.materialsSelected.push({
            material:   '',
            percentage: null,
            touchable:  true
        });
    };

    // var prevID = $scope.Product.merchantProductId;
    // $scope.createVariation = function (colors, sizes) {
    //     var skuList = $scope.productVariations;
    //     $scope.productVariations = [];
    //     var pushed = {};
    //     if (sizes && colors && skuList) {
    //         for (var i = 0; i < colors.length; i++) {
    //
    //             for (var j = 0; j < sizes.length; j++) {
    //                 var sku = '';
    //                 var checkPush = true;
    //                 for (var a = 0; a < skuList.length; a++) {
    //                     if ((skuList[a].color == colors[i].color) && (skuList[a].size == sizes[j].text)) {
    //                         sku = skuList[a].sku;
    //                         sku = (sku == prevID + "-" + colors[i].color.toLowerCase() + "-" + sizes[j].text.toLowerCase()) ? '' : sku;
    //                     }
    //                 }
    //
    //                 if (!sku) {
    //                     sku = ($scope.Product.merchantProductId ? $scope.Product.merchantProductId : 'id') + "-" + colors[i].color.toLowerCase() + "-" + sizes[j].text.toLowerCase();
    //                 }
    //
    //                 pushed = {
    //                     'sku':   sku,
    //                     'color': colors[i].color,
    //                     'size':  sizes[j].text,
    //                     'stock': 0
    //                 };
    //                 for (var c = 0; c < $scope.productVariationsUntouchebles.length; c++) {
    //                     if ((colors[i].color == $scope.productVariationsUntouchebles[c].color) &&
    //                         (sizes[j].text == $scope.productVariationsUntouchebles[c].size)) {
    //                         checkPush = false;
    //                     }
    //                 }
    //                 if (checkPush) {
    //                     $scope.productVariations.push(pushed);
    //                 }
    //
    //             }
    //         }
    //     } else {
    //         // TODO Or else?
    //     }
    //     prevID = $scope.Product.merchantProductId;
    // };

    $scope.generateVariations = function () {
        console.log("Generate Variations");
        if ($scope.Product.merchantProductId && $scope.Product.merchantProductId !== "" && $scope.Product.colorsSelected[0].color !== "" && $scope.Product.sizesSelected !== undefined) {
            $scope.Product.productVariations = [];
            angular.forEach($scope.Product.colorsSelected, function (color, color_key) {
                angular.forEach($scope.Product.sizesSelected, function (size, size_key) {
                    var checkPush = true;

                    angular.forEach($scope.Product.productVariationsUntouchebles, function (untouch, untouch_key) {
                        var untouch_size = untouch.sizeSelect;
                        if ($scope.Product.sizesType !== "International") {
                            untouch_size = untouch.sizeSelect.split(" ")[1];
                        }

                        if (color.color == untouch.color && size.text == untouch.size) {
                            checkPush = false;
                        }
                    });

                    if (checkPush) {
                        var sku = $scope.Product.merchantProductId.toUpperCase() + "-" + color.color.toUpperCase() + "-" + $scope.Product.sizesType + size.text.toString().toUpperCase();

                        var pushed = {
                            sku:   sku,
                            color: color.color,
                            size:  size.text,
                            stock: 0
                        };
                        $scope.Product.productVariations.push(pushed);
                    }
                });
            });
        }
        else {
            console.log("Need more data for variations id/color/size");
            return false;
        }
    };

    $scope.$watch('Product.sizesSelected', function () {
        $scope.generateVariations();
    });

    // TODO Is this the best way?
    uploadData();
    function uploadData() {

        $http.get(config.apiUrl + '/admin/s3policy/some_url/').then(
            function (response) {
                $scope.s3 = response.data;
            }
        );
    }



    $scope.saveImages = function (files) {
        /*jshint -W083 */
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.$error) {
                    Upload.upload({
                        url:               '//' + $scope.s3.url, //S3 upload url including bucket name
                        method:            'POST',
                        skipAuthorization: true,
                        data:              {
                            key:            file.name, // the key to store the file on S3, could be file name or customized
                            AWSAccessKeyId: $scope.s3.aws,
                            acl:            'public-read', // sets the access to the uploaded file in the bucket: private, public-read, ...
                            policy:         $scope.s3.policy, // base64-encoded json policy (see article below)
                            signature:      $scope.s3.signature, // base64-encoded signature based on policy string (see article below)
                            "Content-Type": file.type !== '' ? file.type : 'application/octet-stream', // content type of the file (NotEmpty)
                            filename:       file.name, // this is needed for Flash polyfill IE8-9
                            file:           file
                        }
                    }).then(function (resp) {
                        console.log(resp);

                        var hasFile = false;
                        var url = '//upload.some_url/';
                        angular.forEach($scope.Product.productImages, function (v, k) {
                            if (resp.config.data.filename === v.name) {
                                hasFile = true;
                            }
                        });
                        if (!hasFile) {
                            $scope.Product.productImages.push({name: resp.config.data.filename, location: url});
                        }
                    });
                }
            }
        }
        /*jshint +W083 */

        angular.forEach(files, function (value, key) {
            var hasFile = false;
            value.location = '//upload.some_url/';
            angular.forEach($scope.Product.productImages, function (v, k) {
                if (value.name === v.name) {
                    hasFile = true;
                }
            });
            if (!hasFile) {
                $scope.Product.productImages.push({name: value.name, location: value.location});
            }
        });
        $scope.generateColorsImages();
    };

    $scope.removeFile = function (index, name) {
        $scope.Product.productImages.splice(index, 1);

        angular.forEach($scope.Product.colorsAndImages.colors, function (color, key) {
            angular.forEach(color.images, function (image, k) {
                if (image.name == name) {
                    $scope.Product.colorsAndImages.colors[key].images.splice(k, 1);
                }
            });
        });

        $scope.generateColorsImages();
    };

    $scope.generateColorsImages = function () {
        if ($scope.Product.productImages && $scope.Product.colorsSelected[0].color !== "") {
            angular.forEach($scope.Product.colorsSelected, function (value, key) {
                var hasColor = false;
                angular.forEach($scope.Product.colorsAndImages.colors, function (v, k) {
                    if (value.color === v.color) {
                        hasColor = true;
                    }
                });
                if (!hasColor && value !== "") {
                    $scope.Product.colorsAndImages.colors.push({color: value.color, images: []});
                }
            });

            angular.forEach($scope.Product.colorsAndImages.colors, function (value, key) {
                var hasColor = false;
                angular.forEach($scope.Product.colorsSelected, function (v, k) {
                    if (value.color === v.color) {
                        hasColor = true;
                    }
                });
                if (!hasColor) {
                    $scope.Product.colorsAndImages.colors.splice(key, 1);
                }
            });

            if ($scope.Product.colorsAndImages.images.length > 0) {
                angular.forEach($scope.Product.productImages, function (value, key) {
                    var hasImage = false;
                    angular.forEach($scope.Product.colorsAndImages.images[0].images, function (v, k) {
                        if (value.name === v.name) {
                            hasImage = true;
                        }
                    });
                    if (!hasImage) {
                        $scope.Product.colorsAndImages.images[0].images.push(value);
                    }
                });

                angular.forEach($scope.Product.colorsAndImages.images[0].images, function (value, key) {
                    var hasImage = false;
                    angular.forEach($scope.Product.productImages, function (v, k) {
                        if (value.name === v.name) {
                            hasImage = true;
                        }
                    });
                    if (!hasImage) {
                        $scope.Product.colorsAndImages.images[0].images.splice(key, 1);
                    }
                });
            }
            else {
                $scope.Product.colorsAndImages.images.push({
                    color:  'Uploaded images',
                    images: $scope.Product.productImages
                });
            }

            if ($scope.Product.colorsAndImages.colors.length > 0 && $scope.Product.colorsAndImages.images[0].images.length > 0) {
                $scope.Product.colorsAndImages.ready = true;
            }
            else {
                $scope.Product.colorsAndImages.ready = false;
            }
        }
        else {
            return false;
        }
    };

    $scope.chosenImage = function (object, image) {
        image.chosen = true;
        return object.images.filter(function (image) {
            return image.chosen;
        });
    };
    $scope.selectedImages = function (object, image) {
        image.selected = true;
        return object.images.filter(function (image) {
            return image.selected;
        });
    };
    $scope.onDrop = function (object, images, index) {
        var haveImages = [];
        var chosenImage = null;

        angular.forEach(images, function (image, key) {
            angular.forEach(object.images, function (oimage, okey) {
                if (image.chosen) {
                    chosenImage = image;
                }

                image.selected = false;
                image.chosen = false;

                if (image.name === oimage.name) {
                    haveImages.push(image.name);
                }
            });
        });

        angular.forEach(haveImages, function (name, key) {
            angular.forEach(images, function (image, k) {
                if (image.name === name) {
                    images.splice(k, 1);
                }
            });
        });

        if (chosenImage) {
            angular.forEach(object.images, function (image, key) {
                if (image.name === chosenImage.name) {
                    object.images.splice(key, 1);
                }
            });
            object.images.splice(index, 0, chosenImage);
        }
        else {
            object.images = object.images.slice(0, index)
                .concat(images)
                .concat(object.images.slice(index));
        }

        return true;
    };
    $scope.drugEnd = function (object) {
        angular.forEach(object.images, function (image, key) {
            image.selected = false;
        });
    };

    $scope.$watch('Product', function (newVal) {
        if (auth.profile.merchantId === undefined || newVal.merchantProductId === undefined || newVal.merchantProductId === "" || newVal.title === undefined || newVal.title === "" || newVal.price === undefined || newVal.price === "" || newVal.mainCategoriesFinal === undefined || newVal.colorsSelected[0].color === "" || newVal.productImages === undefined || newVal.sizesSelected === undefined) {
            $scope.canSave = false;
        }
        else if (newVal.mainCategoriesFinal !== undefined && newVal.productImages !== undefined && newVal.sizesSelected !== undefined) {
            if (newVal.mainCategoriesFinal.length < 1 || newVal.productImages.length < 1 || newVal.sizesSelected.length < 1) {
                $scope.canSave = false;
            }
            else {
                $scope.canSave = true;
            }
        }
    }, true);

    $scope.saveProduct = function () {
        var toPost = {};

        toPost.id = null;
        if ($scope.Product.id !== undefined && $scope.edition) {
            toPost.id = $scope.Product.id;
        }

        toPost.merchantId = $scope.Merchant.id;

        toPost.title = $scope.Product.title;

        toPost.tags = [];
        if ($scope.Product.tags !== undefined) {
            angular.forEach($scope.Product.tags, function (tag, key) {
                toPost.tags.push(tag.text);
            });
        }

        toPost.metaTitle = null;
        if ($scope.Product.metaTitle !== undefined && $scope.Product.metaTitle !== "") {
            toPost.metaTitle = $scope.Product.metaTitle;
        }

        toPost.metaDescription = null;
        if ($scope.Product.metaDescription !== undefined && $scope.Product.metaDescription !== "") {
            toPost.metaDescription = $scope.Product.metaDescription;
        }

        toPost.metaTags = null;
        if ($scope.Product.metaTags !== undefined) {
            toPost.metaTags = [];
            angular.forEach($scope.Product.metaTags, function (tag, key) {
                toPost.metaTags.push(tag.text);
            });
        }

        toPost.categories = [];
        toPost.categories.push($scope.Product.gender + '|' + $scope.Product.firstCategory + '|' + $scope.Product.secondCategory);
        angular.forEach($scope.Product.mainCategoriesFinal, function (category, key) {
            if (category.ticked) {
                toPost.categories.push($scope.Product.gender + '|' + $scope.Product.firstCategory + '|' + $scope.Product.secondCategory + '|' + category.text);
            }
        });

        toPost.price = $scope.Product.price;

        toPost.originalPrice = null;
        if ($scope.Product.originalPrice !== undefined) {
            toPost.originalPrice = $scope.Product.originalPrice;
        }

        toPost.shippingCost = null;
        if ($scope.Product.shippingCost !== undefined) {
            toPost.shippingCost = $scope.Product.shippingCost;
        }

        toPost.currency = "GBP";

        toPost.material = [];
        angular.forEach($scope.Product.materialsSelected, function (material, key) {
            toPost.material.push({
                name:       material.material,
                percentage: material.percentage
            });
        });

        toPost.merchantProductId = $scope.Product.merchantProductId;

        toPost.colors = [];
        angular.forEach($scope.Product.colorsAndImages.colors, function (color, key) {
            var colorImages = [];
            angular.forEach(color.images, function (image, k) {
                colorImages.push(image.name);
            });
            toPost.colors.push({
                color:  color.color,
                images: colorImages
            });
        });

        toPost.variations = [];
        var allVariations = [];
        if ($scope.Product.productVariationsUntouchebles.length > 0) {
            allVariations = $scope.Product.productVariations.concat($scope.Product.productVariationsUntouchebles);
            allVariations.sort(function (a, b) {
                return (a.color > b.color) ? 1 : ((b.color > a.color) ? -1 : 0);
            });
        }
        else {
            allVariations = $scope.Product.productVariations;
        }
        angular.forEach(allVariations, function (variation, key) {
            var object = {};
            if ($scope.Product.sizeType === "International") {
                object.sizeSelect = variation.size;
            } else {
                object.sizeSelect = $scope.Product.sizesType + " " + variation.size;
            }
            object.color = variation.color;
            object.sku = variation.sku;
            object.stock = variation.stock;
            angular.forEach($scope.Product.colorsAndImages.colors, function (color, ckey) {
                if (object.color === color.color) {
                    object.images = [];
                    angular.forEach(color.images, function (image, ikey) {
                        if (image.location === "//upload.some_url/") {
                            object.images.push("s3|" + image.name);
                        } else {
                            object.images.push(image.name);
                        }
                    });
                }
            });
            toPost.variations.push(object);
        });

        toPost.images = [];
        angular.forEach($scope.Product.productImages, function (image, key) {
            if (image.location === "//upload.some_url/") {
                toPost.images.push("s3|" + image.name);
            } else {
                toPost.images.push(image.name);
            }
        });

        console.log(toPost);
        console.log(JSON.stringify(toPost));

        $http.post(config.apiUrl + "/product/new/publish", toPost).then(
            function success(response) {
                console.log("Data sent");
                toastr.success('Data changes were sent to server. Please, take a note that changes will take some time to be visible.', 'Request successful!', {progressBar: true});
                setTimeout(function () {
                    $state.go('product.list');
                }, 5000);
            },
            function error(response) {
                $scope.loading = false; // stop loading
                Bugsnag.notify("API communication error", response.message, response);
                toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
            });

    };
}
