function CategoriesEditCtrl(config, $scope, $http, toastr) {
    $scope.remove = function (scope) {
        scope.remove();
    };

    $scope.toggle = function (scope) {
        scope.toggle();
    };

    $scope.moveLastToTheBeginning = function () {
        var a = $scope.data.pop();
        $scope.data.splice(0, 0, a);
    };

    $scope.newSubItem = function (scope) {
        var nodeData = scope.$modelValue;
        nodeData.nodes.push({
            id:    nodeData.id + "|New Node",
            title: "New Node",
            nodes: []
        });
    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.save = function () {
        $http.post(config.apiUrl + "/admin/data/categories", $scope.data).then(
            function success(response) {
                console.log("Categories data sent");
                toastr.success('Categories changes were sent to server.', 'Request successful!', {progressBar: true});
            },
            function error(response) {
                console.log("Categories data NOT sent", JSON.stringify(response));
            }
        );
    };

    $scope.data = null;
    $http.get('categories_edit.json', {cache: true}).then(function (response) {
        $scope.data = response.data;
    });
}