function ordersActionCtrl(config, $http, toastr, $scope, $uibModalInstance, action, items, checklist) {

    $scope.view = action;
    $scope.loading = false;

    console.log(items);

    $scope.products = [];
    angular.forEach(items, function (value, key) {
        angular.forEach(value.orderedProducts, function (v, k) {
            if (v.status === null) {
                v.subOrderId = value.subOrderId;
                $scope.products.push(v);
            }
        });
    });

    $scope.toShow = checklist;
    if ($scope.toShow == 'checkedAll') {
        $scope.toShow = [];
        for (var j = 0; j < $scope.products.length; j++) {
            $scope.toShow.push(true);
        }
    }

    $scope.close = function (response) {
        if (response) {
            $uibModalInstance.close(response.data);
        }
        else {
            $uibModalInstance.close();
        }
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.sendMassAction = function (action) {
        var toPost = {};
        toPost.items = {};
        if (action === 'Mass ship edit') {
            angular.forEach(items, function(item, key) {
                var oneOrderProducts = [];
                angular.forEach($scope.products, function(product, k) {
                    if ($scope.toShow[k] && product.subOrderId === item.subOrderId) {
                        oneOrderProducts.push({
                            "productId" : product.productId,
                            "sku" : product.sku,
                            "amount" : product.amount
                        });
                    }
                });
                toPost.items[item.subOrderId] = oneOrderProducts;
            });
            console.log(toPost);
            console.log(JSON.stringify(toPost));
            $http.post(config.apiUrl + "/admin/order/ship", toPost).then(
                function success(response) {
                    console.log("Data sent", response);
                    toastr.success('Data changes were sent to server.', 'Request successful!', {progressBar: true});
                    $scope.close(response);
                },
                function error(response) {
                    $scope.loading = false; // stop loading
                    Bugsnag.notify("API communication error", response.message, response);
                    toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
                });
        }
    };
}

