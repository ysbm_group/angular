function RegistrationModalCtrl(config, $scope, $http, $uibModalInstance) {
    $scope.registration = {};

    $scope.registration.register = function (data, event) {
        console.log("--> Submiting form:\n" + JSON.stringify(data) + "\n" + event);
        $http.post(config.apiUrl + "/shop/register", data).then(
            function success(response) {
                console.log("Registration data sent");
                $uibModalInstance.close(true);
            },
            function error(response) {
                console.log("Registration data NOT sent", JSON.stringify(response));
                if (response.status == 409) { // Conflict
                    $scope.error = response.data.message;
                }
                //$uibModalInstance.close();
            }
        );
    };
}

function LandingPageCtrl(config, $scope, $uibModal, toastr) {

    $scope.open = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/page/registration.html',
            controller:  'RegistrationModalCtrl'
        });

        modalInstance.result.then(function (result) {
            console.log("Returned result: " + JSON.stringify(result));
            if (result) {
                toastr.success('A request to create an account was successfully filed with our staff.', 'Request successfull!', {progressBar: true});
            }
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };
}

function OrdersListCtrl(config, $scope, $state, $http, toastr, $uibModal) {
    //$scope.dateRange = {startDate: '2015/01/01', endDate: moment()};
    //var ordersList = {};
    //$scope.orderToSend = {};
    //$scope.dateRangeOpts = {
    //    ranges: {
    //        'Today':        [moment(), moment()],
    //        'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //        'Last 7 Days':  [moment().subtract(6, 'days'), moment()],
    //        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //        'This Month':   [moment().startOf('month'), moment().endOf('month')],
    //        'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //    }
    //};

    $http.get(config.apiUrl + "/admin/order", {cache: false}).then(function success(response) {
        console.log("Got data:\n" + JSON.stringify(response.data._source));
        var orders = response.data;
        $scope.orders = orders;
        $http.get(config.apiUrl + "/admin/shop/" + $scope.orders[0].merchantId, {cache: true}).then(function success(response) {
            $scope.data = response.data;
            console.log(response.data);
        });
    }, function error(response) {
        $scope.error = 'Something went wrong';
        console.log("Error: " + response.statusText);
    });

    $scope.processOrder = function (data, orderID) {
        console.log(data);
        $http.get(config.apiUrl + "/admin/order/" + orderID, {cache: false}).then(function success(response) {
            console.log("Got data:\n" + JSON.stringify(response.data));
            var orderGot = response.data;
            $scope.orderToSend = orderGot;
            $uibModal.open({
                animation:   true,
                templateUrl: 'views/order/itemsModal.html',
                controller:  'itemsActionCtrl',
                size:        'xs',
                resolve:     {
                    action:    function () {
                        return data;
                    },
                    items:     function () {
                        return $scope.orderToSend;
                    },
                    checklist: function () {
                        return 'checkedAll';
                    }
                }
            });
        }, function error(response) {
            console.log("Error: " + response.statusText);
        });
    };
    $scope.contactCustomer = function (name) {
        $uibModal.open({
            animation:   true,
            templateUrl: 'views/emailModal.html',
            controller:  'emailCtrl',
            size:        'xs',
            resolve:     {
                username: function () {
                    return name;
                }
            }
        });
    };
    $scope.searchOrders = function (data, dates) {
        if (data) {
            var query = {
                subOrderId:   "" + data.orderId,
                customerName: "" + data.customer,
                orderStatus:  "" + data.status
            };
            $http.post(config.apiUrl + "/admin/order", query).then(function success(response) {
                $scope.orders = response.data;
                console.log(response.data);
            });
            console.log(query);
        }
    };

    $scope.ordersSelected = [];
    $scope.ordersCount = 0;

    $scope.checkCount = function (index) {
        if ($scope.ordersSelected[index] !== false) {
            $scope.ordersCount++;
        }
        else $scope.ordersCount--;
    };
    $scope.massShipAction = function () {
        var neededOrders = [];
        angular.forEach($scope.orders, function (value, key) {
            angular.forEach($scope.ordersSelected, function (v, k) {
                // if (value.subOrderId == v && value.orderStatus == "NEW") {
                if (value.subOrderId == v) {
                    neededOrders.push(value);
                }
            });
        });
        $uibModal.open({
            animation:   true,
            templateUrl: 'views/order/ordersActionModal.html',
            controller:  'ordersActionCtrl',
            size:        'xs',
            resolve:     {
                action:    function () {
                    return 'Mass ship edit';
                },
                items:     function () {
                    return neededOrders;
                },
                checklist: function () {
                    return 'checkedAll';
                }
            }
        });
    };
}

function RefundsListCtrl(config, $scope, $http, toastr, $uibModal) {
    //$scope.dateRange = {startDate: '2015/01/01', endDate: moment()};
    //$scope.orderToSend = {};
    //$scope.dateRangeOpts = {
    //    ranges: {
    //        'Today':        [moment(), moment()],
    //        'Yesterday':    [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //        'Last 7 Days':  [moment().subtract(6, 'days'), moment()],
    //        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //        'This Month':   [moment().startOf('month'), moment().endOf('month')],
    //        'Last Month':   [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //    }
    //};

    $http.get(config.apiUrl + "/admin/refund", {cache: false}).then(function success(response) {
        console.log("Got data:\n" + JSON.stringify(response.data._source));
        var orders = response.data;
        console.log(response.data);
        $scope.refunds = orders;
    }, function error(response) {
        console.log("Error: " + response.statusText);
    });

    $scope.refundOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation:   true,
            templateUrl: 'views/order/refundsModal.html',
            controller:  'itemsActionCtrl',
            size:        'xs',
            resolve:     {
                action:    function () {
                    return 'Refund';
                },
                items:     function () {
                    return data;
                },
                checklist: function () {
                    return 'checkedAll';
                }
            }
        });
        modalInstance.result.then(function (income) {
            for (var z = 0; z < $scope.refunds.length; z++) {
                if ($scope.refunds[z].subOrderId == income.subOrderId) {
                    $scope.refunds[z].status = 'REFUNDED';
                }
            }
        }, function () {
        });
    };

    $scope.searchOrders = function (data, dates) {
        if (data) {
            var query = {
                subOrderId:   "" + data.orderId,
                customerName: "" + data.customer,
                orderStatus:  "" + data.status
            };
            $http.post(config.apiUrl + "/admin/order", query).then(function success(response) {
                $scope.orders = response.data;
                console.log(response.data);
            });
            console.log(query);
        }
    };

}

function ProductImportCtrl(config, $scope, $http, toastr) {
    $scope.files = [];
}

function MerchantDetailsCtrl(config, $scope, $http, $stateParams) {
    if ($stateParams.merchantID) {
        $http.get(config.apiUrl + "/admin/shop/" + $stateParams.merchantID, {cache: true}).then(function success(response) {
            $scope.data = response.data;
            console.log(response.data);
        });
    }

    $scope.activate = function (data, event) {
    };
}

function OrderDetailsCtrl(config, $scope, $http, $stateParams, $uibModal) {
    /* ATTENTION THIS DICTIONARY MAY IN AN UPDATE */
    $scope.dictionary = {
        "WRONG_QUANTITY":         "Wrong quantity",
        "CHANGED_MY_MIND":        "Changed my mind",
        "BETTER_PRICE_AVAILABLE": "Better price available",
        "DELIVERY_ADDRESS_WRONG": "Delivery address is wrong",
        "ORDERED_MISTAKE":        "Ordered by mistake",
        "OUT_OF_STOCK":           "Out of stock",
        "INVALID_ADDRESS":        "Invalid address"
    };

    $scope.imageSource = [];
    $scope.imageSourceCancelled = [];
    $scope.itemNumber = [];

    for (var c = 0; c < 6; c++) {
        getRandomInt(0, 10);
    }
    var showAddition = 0;
    $scope.showAddition = 0;
    var merchantID;

    $http.get(config.apiUrl + "/admin/order/" + $stateParams.orderId, {cache: true}).then(function success(response) {
        console.log(response.data);
        $scope.order = response.data;

        /* ATTENTION THIS VARIABLE MAY BE CHANGED IN FUTURE */
        $scope.order.sellerFeeSum = 0;

        merchantID = $scope.order.merchantId;
        if (merchantID) {
            $http.get(config.apiUrl + "/admin/shop/" + merchantID, {cache: true}).then(function success(response) {
                $scope.data = response.data;
                console.log(response.data);
            });
        }

        $scope.invoiceNumber = $scope.order.subOrderId.substr(0, 8);
        // for (var c = 0; c < 6; c++){
        //     var invoiceNumber = getRandomInt(0, 10);
        //     $scope.invoiceNumber = $scope.invoiceNumber + invoiceNumber;
        // }
    }, function error(response) {
        console.log("Error: " + response.statusText);
    });

    $scope.checkAll = function () {
        $scope.itemNumber = [];
        $scope.showAddition = 0;
        for (var i = 0; i < $scope.order.orderedProducts.length; i++) {
            $scope.itemNumber.push(true);
            $scope.showAddition = $scope.showAddition + 1;
        }
        console.log($scope.itemNumber);
    };

    $scope.removeItems = function (data, list, checked) {
        console.log(checked);
        var itemModal = $uibModal.open({
            animation:   true,
            templateUrl: 'views/order/itemsModal.html',
            controller:  'itemsActionCtrl',
            size:        'lg',
            resolve:     {
                action:    function () {
                    return data;
                },
                items:     function () {
                    return list;
                },
                checklist: function () {
                    return checked;
                }
            }
        });

        itemModal.result.then(function (subOrder) {
            $scope.order = subOrder;
            $scope.itemNumber = [];
            $scope.showAddition = 0;
        }, function () {
            log.info('Modal dismissed without action at: ' + new Date());
        });
    };

    $scope.callMeMayBe = function (username) {
        console.log(username);
        $uibModal.open({
            animation:   true,
            templateUrl: 'views/emailModal.html',
            controller:  'emailCtrl',
            size:        'xs',
            resolve:     {
                username: function () {
                    return username;
                }
            }
        });
    };

}
function getRandomInt(min, max) {
    return (Math.floor(Math.random() * (max - min)) + min).toString();
}
function emailCtrl(config, $http, toastr, $scope, $uibModalInstance, username, $log) {
    $scope.loading = false; // stop loading
    $scope.send = function () {
        var mailJSON = {
            "recipient": "" + username,
            "subject":   "" + $scope.subject,
            "message":   "" + $scope.text
        };
        $scope.loading = true; // start loading
        console.log(mailJSON);
        $http.post(config.apiUrl + '/admin/shop/contactuser', mailJSON)
            .success(function (response) {
                toastr.success('Email was sent to server.', 'Request successful!', {progressBar: true});
                $scope.close();
            })
            .error(function (response) {
                $scope.loading = false; // stop loading
                Bugsnag.notify("API communication error", response.message, response);
                toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
            });
    };

    $scope.close = function (response) {
        $log.log('close');
        if (response) {
            $uibModalInstance.close(response.data);
        }
        else {
            $uibModalInstance.close();
        }
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

function LoginCtrl(config, $scope, auth, $state, store) {
    auth.signin({
        authParams:                    {scope: 'openid name email userType merchantId user_metadata app_metadata picture'},
        icon:                          'https://some_url/res/icon_58.png',
        defaultUserPasswordConnection: config.auth0connection
    }, function (profile, token) {
        store.set('profile', profile);
        store.set('token', token);
        $state.go('admin.main');
    });
}

function LogoutCtrl(auth, $location, store, $scope, $state) {
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $state.go('index');

}

function ResetPasswordCtrl(config, auth, $location, store, $scope, $state) {
    auth.reset({
        icon:                          'https://some_url/res/icon_58.png',
        defaultUserPasswordConnection: config.auth0connection
    });
    store.remove('profile');
    store.remove('token');
}

function productChangeModalCtrl(config, $scope, $http, $uibModalInstance, toastr, item) {
    $scope.productId = item;
    $scope.pricePattern = /[0-9\.]/;

    $http.get(config.apiUrl + "/product/" + item + "/details", {cache: false}).then(function success(response) {
        $scope.product = response.data;
        console.log("Product", response.data);
    });

    $scope.submitChange = function () {
        var toPost = {};
        toPost.price = $scope.product.price;
        toPost.originalPrice = $scope.product.originalPrice;
        toPost.variations = [];

        angular.forEach($scope.product.variations, function (variation) {
            toPost.variations.push({sku: variation.sku, stock: variation.stock});
        });

        console.log(toPost);

        $http.post(config.apiUrl + "/product/" + item + "/quickedit", toPost).then(
            function success(response) {
                console.log("Data sent");
                toastr.success('Data changes were sent to server. Please, take a note that changes will take some time to be visible.', 'Request successful!', {progressBar: true});
                $uibModalInstance.close($scope.product);
            },
            function error(response) {
                $scope.loading = false; // stop loading
                Bugsnag.notify("API communication error", response.message, response);
                toastr.error('There was a problem communicating with the backend.', 'If the problem persists, please contact support!', {progressBar: true});
            });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

angular
    .module('atelier')
    .controller('MainCtrl', MainCtrl)
    .controller('LoginCtrl', LoginCtrl)
    .controller('LogoutCtrl', LogoutCtrl)
    .controller('ResetPasswordCtrl', ResetPasswordCtrl)
    .controller('LandingPageCtrl', LandingPageCtrl)
    .controller('RegistrationModalCtrl', RegistrationModalCtrl)
    .controller('ProductListCtrl', ProductListCtrl)
    .controller('PreviewWindowCtrl', ProductPreviewModalCtrl)
    .controller('itemsActionCtrl', itemsActionCtrl)
    .controller('ProductEditCtrl', ProductEditCtrl)
    .controller('ProductImportCtrl', ProductImportCtrl)
    .controller('CategoriesEditCtrl', CategoriesEditCtrl)
    .controller('OrdersListCtrl', OrdersListCtrl)
    .controller('RefundsListCtrl', RefundsListCtrl)
    .controller('OrderDetailsCtrl', OrderDetailsCtrl)
    .controller('MerchantDetailsCtrl', MerchantDetailsCtrl)
    .controller('ShopDetailsCtrl', ShopDetailsCtrl)
    .controller('productChangeModalCtrl', productChangeModalCtrl)
    .controller('emailCtrl', emailCtrl)
    .controller('ordersActionCtrl', ordersActionCtrl);
