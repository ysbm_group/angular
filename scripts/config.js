/**
 * Atelier app use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Be careful with ocLazyLoad - it's tricky and sometimes the plugins are loaded in specific JS files!!!
 */
function config($stateProvider, $urlRouterProvider, $httpProvider, $ocLazyLoadProvider, $locationProvider, authProvider, jwtInterceptorProvider, $analyticsProvider) {
    $analyticsProvider.virtualPageviews(false);

    $locationProvider.html5Mode(true).hashPrefix('!');

    $ocLazyLoadProvider.config({
        /* remove-for-production */
        // Set to true if you want to see what and when is dynamically loaded
        debug:   true,
        /* end-remove-for-production */
        modules: [
            {
                name:  'elasticsearch',
                files: ['bower_components/elasticsearch/elasticsearch.angular.js']
            },
            {
                name:  'elasticui',
                files: ['bower_components/elastic.js/dist/elastic.js', 'bower_components/elasticui/dist/elasticui.js']
            },
            {
                name:  'xeditable',
                files: ['bower_components/angular-xeditable/dist/js/xeditable.js', 'bower_components/angular-xeditable/dist/css/xeditable.css']
            },
            {
                name:  'ngTagsInput',
                files: ['bower_components/ng-tags-input/ng-tags-input.js', 'bower_components/ng-tags-input/ng-tags-input.css', 'bower_components/ng-tags-input/ng-tags-input.bootstrap.css']
            },
            {
                name:  'nk.touchspin',
                files: ['bower_components/angular-touchspin/dist/angular-touchspin.js', 'bower_components/angular-touchspin/dist/angular-touchspin.css']
            },
            {
                name:  'ui.tree',
                files: ['bower_components/angular-ui-tree/dist/angular-ui-tree.css', 'bower_components/angular-ui-tree/dist/angular-ui-tree.js']
            },
            {
                name:  'isteven-multi-select',
                files: ['bower_components/isteven-angular-multiselect/isteven-multi-select.js', 'bower_components/isteven-angular-multiselect/isteven-multi-select.css']
            },
            {
                name:  'ngFileUpload',
                files: ['bower_components/ng-file-upload/ng-file-upload.js', 'bower_components/ng-file-upload/ng-file-upload-shim.js']
            },
            {
                name:  'daterangepicker',
                files: ['bower_components/bootstrap-daterangepicker/daterangepicker.js', 'bower_components/bootstrap-daterangepicker/daterangepicker.css', 'bower_components/angular-daterangepicker/js/angular-daterangepicker.js']
            },
            {
                name:  'validate',
                files: ['bower_components/angular-validation/dist/angular-validation.js', 'bower_components/angular-validation/dist/angular-validation-rule.js']
            },
            {
                name:  'dndLists',
                files: ['bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js']
            }
        ]
    });

    $urlRouterProvider.otherwise("/index");
    $stateProvider
        .state('index', {
            url:         "/index",
            templateUrl: "views/page/landing.html",
            data:        {pageTitle: "Index", specialClass: "landing-page"}
        })
        .state('login', {
            url:        '/login',
            controller: 'LoginCtrl'
        })
        .state('logout', {
            url:        '/logout',
            controller: 'LogoutCtrl'
        })
        .state('resetPassword', {
            url:        '/resetPassword',
            controller: 'ResetPasswordCtrl'
        })
        .state('admin', {
            abstract:    true,
            url:         "/admin",
            templateUrl: "views/common/content.html"
        })
        .state('admin.main', {
            url:         "/main",
            templateUrl: "views/main.html",
            data:        {pageTitle: "Dashboard", requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name:  'angular-flot',
                            files: ['bower_components/flot/jquery.flot.js', 'bower_components/flot/jquery.flot.time.js', 'bower_components/flot.tooltip.pib/js/jquery.flot.tooltip.js', 'bower_components/jquery.flot.spline/index.js', 'bower_components/flot/jquery.flot.resize.js', 'bower_components/flot/jquery.flot.pie.js', 'bower_components/flot.curvedlines/curvedLines.js', 'bower_components/angular-flot/angular-flot.js',]
                        },
                        {
                            files: ['bower_components/jquery.sparkline/index.js']
                        }
                    ]);
                }
            }

        })
        .state('product', {
            abstract:    true,
            url:         "/product",
            templateUrl: "views/common/content.html",
        })
        .state('product.list', {
            url:         "/list",
            templateUrl: "views/product/list.html",
            data:        {pageTitle: 'Product listing'}, requiresLogin: true,
            resolve:     {
                loadPlugin: function ($ocLazyLoad, $injector) {
                    return $ocLazyLoad.load([
                        'elasticui', 'xeditable', 'nk.touchspin'
                    ]);
                }
            }
        })
        .state('product.edit', {
            url:         "/edit/{productID}",
            templateUrl: "views/product/edit.html",
            data:        {pageTitle: 'Edit product', requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'ngTagsInput', 'nk.touchspin', 'isteven-multi-select', 'ngFileUpload', 'dndLists',
                        {
                            files: ['bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.js', 'bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.css']
                        }
                    ]);
                }
            }

        })
        .state('product.add', {
            url:         "/add",
            templateUrl: "views/product/edit.html",
            data:        {pageTitle: 'Add new product', requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'ngTagsInput', 'nk.touchspin', 'isteven-multi-select', 'ngFileUpload', 'dndLists',
                        {
                            files: ['bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.js', 'bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.css']
                        }
                    ]);
                }
            }
        })
        .state('product.import', {
            url:         "/upload",
            templateUrl: "views/product/import.html",
            data:        {pageTitle: "Import files", requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'ngFileUpload'
                    ]);
                }
            }
        })
        .state('orders', {
            abstract:    true,
            url:         "/orders",
            templateUrl: "views/common/content.html"
        })
        .state('orders.active', {
            url:         "/list",
            templateUrl: "views/order/list.html",
            data:        {pageTitle: 'Active orders', requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'daterangepicker'
                    ]);
                }
            }
        })
        .state('orders.edit', {
            url:         "/edit/{orderId}",
            templateUrl: "views/order/edit.html",
            data:        {pageTitle: 'Edit order', requiresLogin: true}
        })
        .state('orders.refunds', {
            url:         "/refunds",
            templateUrl: "views/order/refunds.html",
            data:        {pageTitle: 'Refunds', requiresLogin: true}
        })
        .state('manage', {
            abstract:    true,
            url:         "/manage",
            templateUrl: "views/common/content.html",
        })
        .state('manage.categories', {
            url:         '/categories',
            templateUrl: 'views/admin/categories.html',
            data:        {
                pageTitle: 'Edit category tree', requiresLogin: true
            },
            resolve:     {
                loadPlugin: function ($ocLazyLoad, $injector) {
                    return $ocLazyLoad.load([
                        'ui.tree', 'xeditable'
                    ]).then(function () {
                        var editableThemes = $injector.get('editableThemes');
                        editableThemes.bs3.inputClass = 'input-sm';
                        editableThemes.bs3.buttonsClass = 'btn-sm';
                        var editableOptions = $injector.get('editableOptions');
                        editableOptions.theme = 'bs3';
                    });
                }
            }
        })
        .state('merchants', {
            abstract:    true,
            url:         "/merchants",
            templateUrl: "views/common/content.html",
        })
        .state('merchants.details', {
            url:         "/show/{merchantID}",
            templateUrl: 'views/merchant/details.html',
            data:        {pageTitle: "Merchant details", requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['bower_components/jquery.sparkline/index.js']
                        },

                    ]);
                }
            }
        })
        .state('shop', {
            abstract:    true,
            url:         "/profile",
            templateUrl: "views/common/content.html",
        })
        .state('shop.settings', {
            url:         "/shop",
            templateUrl: 'views/shop/settings.html',
            data:        {pageTitle: "Shop settings", requiresLogin: true},
            resolve:     {
                loadPlugin: function ($ocLazyLoad, $injector) {
                    return $ocLazyLoad.load([
                        'ngFileUpload'
                    ]);
                }
            }
        });
}

angular
    .module('atelier')
    .config(config)
    .run(function ($rootScope, $state, auth, store, jwtHelper) {
        $rootScope.$state = $state;

        auth.hookEvents();
        $rootScope.$on('$locationChangeStart', function () {
            if (!auth.isAuthenticated) {
                var token = store.get('token');

                if (token) {
                    if (!jwtHelper.isTokenExpired(token)) {
                        auth.authenticate(store.get('profile'), token);
                    } else {
                        $state.go('login');
                    }
                }
            }
        });
    });
