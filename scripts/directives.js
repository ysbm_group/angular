/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function (scope, element) {
            var listener = function (event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'trendeo | Fashion gateway';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'trendeo | ' + toState.data.pageTitle;
                $timeout(function () {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    };
}

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link:     function (scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function () {
                element.metisMenu();
            });
        }
    };
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict:    'A',
        scope:       true,
        templateUrl: 'views/common/ibox_tools.html',
        controller:  function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
 */
function minimalizaSidebar($timeout) {
    return {
        restrict:   'A',
        template:   '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')) {
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            };
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict:    'A',
        scope:       true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller:  function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            };
        }
    };
}

/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope:    {
            sparkData:    '=',
            sparkOptions: '=',
        },
        link:     function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function () {
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    };
}

/*
 * ionRangeSlider - if we're using it.
 */
//function ionRangeSlider() {
//    return {
//        restrict: "E",
//        scope: {
//            min: "=",
//            max: "=",
//            type: "@",
//            prefix: "@",
//            maxPostfix: "@",
//            prettify: "@",
//            grid: "@",
//            gridMargin: "@",
//            postfix: "@",
//            step: "@",
//            hideMinMax: "@",
//            hideFromTo: "@",
//            from: "=",
//            to: "=",
//            disable: "=",
//            onChange: "=",
//            onFinish: "="
//        },
//        replace: true,
//        link: function ($scope, $element) {
//            $element.ionRangeSlider({
//                min: $scope.min,
//                max: $scope.max,
//                type: $scope.type,
//                prefix: $scope.prefix,
//                maxPostfix: $scope.maxPostfix,
//                prettify: $scope.prettify,
//                grid: $scope.grid,
//                gridMargin: $scope.gridMargin,
//                postfix: $scope.postfix,
//                step: $scope.step,
//                hideMinMax: $scope.hideMinMax,
//                hideFromTo: $scope.hideFromTo,
//                from: $scope.from,
//                to: $scope.to,
//                disable: $scope.disable,
//                onChange: $scope.onChange,
//                onFinish: $scope.onFinish
//            });
//            var watchers = [];
//            watchers.push($scope.$watch("min", function (value) {
//                $element.data("ionRangeSlider").update({
//                    min: value
//                });
//            }));
//            watchers.push($scope.$watch('max', function (value) {
//                $element.data("ionRangeSlider").update({
//                    max: value
//                });
//            }));
//            watchers.push($scope.$watch('from', function (value) {
//                $element.data("ionRangeSlider").update({
//                    from: value
//                });
//            }));
//            watchers.push($scope.$watch('disable', function (value) {
//                $element.data("ionRangeSlider").update({
//                    disable: value
//                });
//            }));
//        }
//    };
//}

function withFloatingLabel() {
    return {
        restrict: 'A',
        link:     function ($scope, $element, attrs) {
            var template = '<div class="floating-label">' + attrs.placeholder + '</div>';

            //append floating label template
            $element.after(template);

            //remove placeholder
            $element.removeAttr('placeholder');

            //hide label tag assotiated with given input
            document.querySelector('label[for="' + attrs.id + '"]').style.display = 'none';

            $scope.$watch(function () {
                if ($element.val().toString().length < 1) {
                    $element.addClass('empty');
                } else {
                    $element.removeClass('empty');
                }
            });
        }
    };
}

/**
 * fullScroll - Directive for slimScroll with 100%
 */
function fullScroll($timeout) {
    return {
        restrict: 'A',
        link:     function (scope, element) {
            $timeout(function () {
                element.slimscroll({
                    height:      '100%',
                    railOpacity: 0.9
                });

            });
        }
    };
}

/**
 * slimScroll - Directive for slimScroll with custom height
 */
function slimScroll($timeout) {
    return {
        restrict: 'A',
        scope:    {
            boxHeight: '@'
        },
        link:     function (scope, element) {
            $timeout(function () {
                element.slimscroll({
                    height:      scope.boxHeight,
                    railOpacity: 0.9
                });

            });
        }
    };
}

function categoryFilter() {
    return function (input, all) {
        if (all) {
            return input.replace(/\|/g, '/');
        } else {
            var path = input.split("\|");
            return path[path.length - 1];
        }
    };
}

function scrollToItem() {
    return {
        restrict: 'A',
        scope:    {
            scrollTo: "@"
        },
        link:     function (scope, $elm, attr) {

            $elm.on('click', function () {
                $('html,body').animate({scrollTop: $(scope.scrollTo).offset().top}, "slow");
                $($(scope.scrollTo).selector).focus();
            });
        }
    };
}

function validNumber() {
    return {
        require: '?ngModel',

        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }

                var clean = val.toString().replace(/[^0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
}
function validString() {
    return {
        require: '?ngModel',
        scope:   {
            filterPattern: '='
        },
        link:    function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                var str;
                if (angular.isUndefined(val)) {
                    val = '';
                }
                var clean = val.replace(scope.filterPattern, '');
                str = clean.substr(0, attrs.ngMaxlength);
                if (val !== str) {
                    ngModelCtrl.$setViewValue(str);
                    ngModelCtrl.$render();
                }
                return str;
            });
        }
    };
}
function printDiv() {
    return {
        restrict: 'A',
        link:     function (scope, element, attrs) {

            var iframe;
            var elementToPrint = document.querySelector(attrs.printDiv);

            if (!window.frames["print-frame"]) {
                var elm = document.createElement('iframe');
                elm.setAttribute('id', 'print-frame');
                elm.setAttribute('name', 'print-frame');
                elm.setAttribute('style', 'display: none;');
                document.body.appendChild(elm);
            }

            function write(value) {
                var doc;
                if (iframe.contentDocument) { // DOM
                    doc = iframe.contentDocument;
                } else if (iframe.contentWindow) { // IE win
                    doc = iframe.contentWindow.document;
                } else {
                    alert('Wonder what browser this is... ' + navigator.userAgent);
                }
                doc.write(value);
                doc.close();
            }

            element.bind('click', function (event) {
                iframe = document.getElementById('print-frame');
                write(elementToPrint.innerHTML);

                if (window.navigator.userAgent.indexOf("MSIE") > 0) {
                    iframe.contentWindow.document.execCommand('print', false, null);
                } else {
                    iframe.contentWindow.focus();
                    iframe.contentWindow.print();
                }
            });
        }
        
    };
}

function printedInfo (){
    return {
        restrict: 'A',
        templateUrl : "views/order/printTemplate.html"
    };
}

function printInvoice (){
    return {
        restrict: 'A',
        templateUrl : "views/order/printInvoice.html"
    };
}

function printInvoiceInner (){
    return {
        restrict: 'A',
        templateUrl : "views/order/printInvoiceInner.html"
    };
}

function printCustomerInvoice (){
    return {
        restrict: 'A',
        templateUrl : "views/order/printCustomerInvoice.html"
    };
}

function integer () {
    var INTEGER_REGEXP = /^\-?\d+$/;
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$validators.integer = function(modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                  // consider empty models to be valid
                  return true;
                }

                if (INTEGER_REGEXP.test(viewValue)) {
                  // it is valid
                  return true;
                }

                // it is invalid
                return false;
            };
        }
    };
}
/**
 *
 * Pass all functions into module
 */
angular
    .module('atelier')
    .filter('category', categoryFilter)
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('sparkline', sparkline)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .directive('fullScroll', fullScroll)
    .directive('slimScroll', slimScroll)
    .directive('scrollToItem', scrollToItem)
    .directive('validNumber', validNumber)
    .directive('validString', validString)
    .directive('printDiv', printDiv)
    .directive('printedInfo', printedInfo)
    .directive('printInvoice', printInvoice)
    .directive('printInvoiceInner', printInvoiceInner)
    .directive('printCustomerInvoice', printCustomerInvoice)
    .directive('integer', integer);
//.directive('ionRangeSlider', ionRangeSlider)
//.directive('withFloatingLabel', withFloatingLabel);
